=====
Currency Convert 
http://drupal.org/project/currency_convert


   
INSTALLATION
------------
Install as you would normally install a contributed 
Drupal module. 
See:https://drupal.org/documentation/install/modules-themes/
modules-7
for further information.

CONFIGURATION
-------------
1. Enable the module
2. To add a currency convert block, 
simply visit that block's configuration page at
Administration > Structure > Blocks
